import Foundation
import RxSwift

//MARK: - Data Fetching
public class RepositoriesListViewModel: NSObject {
    public var repositories = Variable<[RepositoriesList]>([])
    func fetchRepositoriesList() {
        APIManager.fetchJavaRepositoriesList().then { repositories -> Void in
            self.repositories.value = repositories
            } .catch { error in
                print(error)
        }
    }
}
