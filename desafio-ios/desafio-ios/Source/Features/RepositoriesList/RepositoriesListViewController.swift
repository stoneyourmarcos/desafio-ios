import UIKit
//import PromiseKit
import RxSwift

class RepositoriesListViewController: UITableViewController {
    fileprivate lazy var viewModel = RepositoriesListViewModel()
    fileprivate lazy var disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel
            .repositories
            .asObservable()
            .subscribe { _ in self.tableView.reloadData() }
            .addDisposableTo(disposeBag)
        viewModel.fetchRepositoriesList()
    }
}


// MARK: - TableView Data Source
extension RepositoriesListViewController {
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repositories.value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoriesListCell",
                                                 for: indexPath) as! RepositoriesListCell
        let repository = viewModel.repositories.value[indexPath.row]
        cell.config(withViewModel: RepositoriesListCellViewModel(withRepository: repository))
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let indexPath = tableView.indexPathForSelectedRow,
            let destination = segue.destination as? PullRequestsFromRepositoryViewController else { return }
        let owner = viewModel.repositories.value[indexPath.row].repositoryOwnerName
        let repository = viewModel.repositories.value[indexPath.row].repositoryName
        destination.ownerPullRequest = owner
        destination.repositoryPullRequest = repository
    }
}
