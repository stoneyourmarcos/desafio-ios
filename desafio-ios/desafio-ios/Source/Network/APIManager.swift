import Moya
import Unbox
import PromiseKit

let apiProvider = MoyaProvider<WebService>()

public class APIManager {}

// MARK: - RepositoriesList
extension APIManager {
    public class func fetchJavaRepositoriesList() -> Promise<[RepositoriesList]>{
        return Promise { fulfill, reject in
            apiProvider.request(.fetchJavaRepositoriesList) { result in
                switch result {
                case let .success(response):
                    do {
                        let data = response.data
                        let json = try JSONSerialization.jsonObject(with: data,
                                                                    options: .mutableContainers)
                        let repository: [RepositoriesList] = try
                            unbox(dictionary: json as! UnboxableDictionary,
                                  atKey: "items")
                        return fulfill(repository)
                    } catch let error {
                        return reject(error)
                    }
                case .failure(_):
                    return reject(NetworkResult.linkError)
                }
            }
        }
    }
}

// MARK: - Repository Pull Requests
extension APIManager {
    public class func fetchPullRequestsFromRepository(owner: String, repository: String) ->
        Promise<[PullRequestsFromRepository]> {
            return Promise { fulfill, reject in
                apiProvider.request(
                .fetchPullRequestsFromRepository(owner: owner, repository: repository)) {
                    result in
                    switch result {
                    case let .success(response):
                        let statusCode = response.statusCode
                        if (statusCode >= 200 && statusCode < 300) {
                            var pullRequests: [PullRequestsFromRepository]?
                            do {
                                pullRequests = try unbox(data: response.data)
                            } catch let error {
                                reject(error)
                                return
                            }
                            fulfill(pullRequests!)
                        }
                        else {
                            return reject(NetworkResult.failToParseResponse)
                        }
                    case .failure(_):
                        reject(NetworkResult.linkError)
                    }
                }
            }
    }
}
