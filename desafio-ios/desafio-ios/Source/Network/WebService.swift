import Moya
import Foundation

public enum WebService {
    case fetchJavaRepositoriesList
     case fetchPullRequestsFromRepository(owner: String, repository: String)
}

extension WebService: TargetType {
    public var baseURL: URL { return URL(string: "https://api.github.com")!}
    
    public var path: String {
        switch self {
        case .fetchJavaRepositoriesList:
            return "/search/repositories"
        case . fetchPullRequestsFromRepository(let owner, let repository):
            return "/repos/\(owner)/\(repository)/pulls"
        }
    }
    
    public var method: Moya.Method { return .get }
    
    public var parameters: [String: Any]? {
        switch self {
        case .fetchJavaRepositoriesList():
            return ["q":"language:java", "sort":"stars","?page":""]
        default:
            return nil
        }
    }
    
    public var parameterEncoding: ParameterEncoding { return URLEncoding.default }
    
    public var sampleData: Data { return Data() }
    
    public var task: Task { return .request }
}
