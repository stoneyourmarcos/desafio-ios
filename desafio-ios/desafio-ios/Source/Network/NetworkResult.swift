// MARK: - Error
enum NetworkResult: Int, CustomStringConvertible, Swift.Error {
    case unkown = 0
    case linkError = 1
    case failToParseResponse = 2
    
    var description: String {
        switch  self {
        case .unkown:
            return "Erro desconhecido"
        case .linkError:
            return "Falha de conexão, tente novamente"
        case .failToParseResponse:
            return "Falha ao parsear a resposta"
        }
    }
}
